# frozen_string_literal: true

require 'strscan'
require 'pp'
require 'benchmark/ips'
require 'benchmark/memory'
require 'cparser'

class Parser
  InvalidInput = Class.new(StandardError)

  def initialize
    @s = StringScanner.new('')
  end

  private

  def scan_author(prefix)
    expect(prefix)

    out = {name: '', email: '', date: 0, timezone: 'UTC'}
    start = @s.pos

    unless @s.skip(/([^\n]*[^ ]) *</)
      out[:name] = rest_of_line
      return out
    end
    out[:name] = @s[1]

    unless @s.skip(/([^\n]*)>/)
      @s.pos = start
      out[:name] = rest_of_line
      return out
    end
    out[:email] = @s[1]

    if @s.skip(/ *([0-9]+)/)
      out[:date] = @s[1].to_i
      out[:timezone] = @s[1] if @s.skip(/ *([+-][0-9]{4})/)
    end

    @s.skip_until(/\n/)
    out
  end

  def scan_line(prefix)
    expect(prefix)
    rest_of_line
  end

  def expect(prefix)
    error("missing #{prefix}") unless @s.skip(prefix)
  end

  def rest_of_line
    line = @s.scan_until(/\n/)
    error("missing newline") unless line
    line.chop!
  end

  def error(msg)
    raise InvalidInput.new(msg)
  end
end

class CommitParser < Parser
  def parse(data)
    @s.string = data
    out = {parents: []}

    out[:tree] = scan_line('tree ')
    out[:parents] << rest_of_line while @s.skip('parent ')
    out[:author] = scan_author('author ')
    out[:committer] = scan_author('committer ')

    @s.skip_until(/\n/) until @s.skip("\n")
    out[:message] = @s.rest

    out
  end
end

class TagParser < Parser
  def parse(data)
    @s.string = data
    out = {}

    out[:object] = scan_line('object ')
    out[:type] = scan_line('type ')
    out[:tag] = scan_line('tag ')
    out[:tagger] = scan_author('tagger ')

    @s.skip_until(/\n/) until @s.skip("\n")

    out[:message] = @s.scan_until(/\n-----BEGIN/)
    if out[:message]
      out[:message].delete_suffix!('-----BEGIN')
      @s.pos -= 10
      out[:signature] = @s.rest
      out[:signed_text] = data[0, @s.pos]
    else
      out[:message] = @s.rest
    end

    out
  end
end

if $0 == __FILE__
  data = $stdin.read.freeze
  parser, c_parser = nil, nil

  case ARGV[0]
  when 'commit'
    parser = CommitParser.new.method(:parse)
    c_parser = GitParser.method(:parse_commit)
  when 'tag'
    parser = TagParser.new.method(:parse)
    c_parser = GitParser.method(:parse_tag)
  end

  exit unless parser

  puts parser.call(data)
  puts c_parser.call(data)

  Benchmark.ips do |x|
    x.report('parse StringScanner') { parser.call(data) }
    x.report('parse C') { c_parser.call(data) }
  end

  Benchmark.memory do |x|
    out = nil
    x.report('parse StringScanner') { out =parser.call(data) }
    x.report('parse C') { out = c_parser.call(data) }
  end
end
