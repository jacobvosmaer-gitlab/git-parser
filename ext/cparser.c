#include <ruby.h>
#include "extconf.h"

static VALUE rb_eParseError;

struct parser {
	const char *start;
	const char *pos;
	const char *end;
};

struct range {
	const char *start;
	const char *end;
};

static VALUE range_to_s(struct range *r) {
	if (r->start > r->end)
		rb_raise(rb_eParseError, "negative range size");
	return rb_str_new(r->start, r->end - r->start);
}

static void consume_prefix(struct parser *parser, const char *prefix, size_t prefix_size) {
	if ((prefix_size > (parser->end - parser->pos)) || 
			memcmp(parser->pos, prefix, prefix_size))
		rb_raise(rb_eParseError, "missing prefix at input position %ld",
			(long) (parser->pos - parser->start)
		);
	parser->pos += prefix_size;
}

static void parser_init(struct parser *parser, VALUE data) {
	Check_Type(data, T_STRING);
	parser->start = StringValuePtr(data);
	parser->pos = parser->start;
	parser->end = parser->pos + RSTRING_LEN(data);
}

#define parser_advance(parser, condition) \
	{ \
		while(((parser)->pos < (parser)->end) && (condition)) \
			(parser)->pos++; \
		if ((parser)->pos >= (parser)->end) \
			rb_raise(rb_eParseError, "parser reached end of input"); \
	}

static int isnum(char c) {
	return (c >= '0') && (c <= '9');
}

static VALUE scan_author(struct parser *parser, const char *prefix, size_t prefix_size) {
	struct range name, email, date, tz;
	char date_buf[16];
	VALUE out = rb_hash_new();

	consume_prefix(parser, prefix, prefix_size);

	name.start = parser->pos;
	parser_advance(parser, (*parser->pos != '<') && (*parser->pos != '\n'));
	if (*parser->pos == '\n') {
		/* garbage, return whole line as name */
		name.end = parser->pos;
		parser->pos++; /* skip over '\n' */
		rb_hash_aset(out, ID2SYM(rb_intern("name")), range_to_s(&name));
		return out;
	}

	name.end = parser->pos;
	while ((name.end > name.start) && (*(name.end - 1) == ' '))
		name.end--;

	parser->pos++; /* skip over '<' */
	email.start = parser->pos;
	parser_advance(parser, (*parser->pos != '>') && (*parser->pos != '\n'));
	if (*parser->pos == '\n') {
		/* garbage email, return whole line as name */
		name.end = parser->pos;
		parser->pos++; /* skip over '\n' */
		rb_hash_aset(out, ID2SYM(rb_intern("name")), range_to_s(&name));
		return out;
	}

	email.end = parser->pos;
	rb_hash_aset(out, ID2SYM(rb_intern("name")), range_to_s(&name));
	rb_hash_aset(out, ID2SYM(rb_intern("email")), range_to_s(&email));

	parser->pos++; /* skip over '>' */
	parser_advance(parser, *parser->pos == ' ');

	date.start = parser->pos;
	parser_advance(parser, isnum(*parser->pos));

	date.end = parser->pos;
	if ((date.end > date.start) && (date.end - date.start < sizeof(date_buf))) {
		memmove(date_buf, date.start, date.end - date.start);
		date_buf[date.end - date.start] = 0; /* strtoumax requires trailing 0 */
		rb_hash_aset(out, ID2SYM(rb_intern("date")),
			INT2NUM(strtoumax(date_buf, NULL, 10))
		);

		parser_advance(parser, *parser->pos == ' ');
		if ((*parser->pos == '+') || (*parser->pos == '-')) {
			tz.start = parser->pos;
			parser->pos++; /* consume '+' or '-' */
			parser_advance(parser, isnum(*parser->pos));
			if (parser->pos - tz.start == 5) {
				tz.end = parser->pos;
				rb_hash_aset(out, ID2SYM(rb_intern("timezone")), range_to_s(&tz));
			}
		}
	}

	parser_advance(parser, *parser->pos != '\n');
	parser->pos++; /* skip over '\n' */
	return out;
}

static VALUE scan_line(struct parser *parser, const char *prefix, size_t prefix_size) {
	struct range line;

	consume_prefix(parser, prefix, prefix_size);
	line.start = parser->pos;
	line.end = memchr(line.start, '\n', parser->end - line.start);
	if (!line.end)
		rb_raise(rb_eParseError, "missing newline");
	parser->pos = line.end + 1;
	return range_to_s(&line);
}

static VALUE parse_commit(VALUE self, VALUE data) {
	struct parser parser;
	struct range message;
	VALUE out = rb_hash_new();
	VALUE parents = rb_ary_new();

	parser_init(&parser, data);

	rb_hash_aset(out, ID2SYM(rb_intern("tree")), scan_line(&parser, "tree ", 5));
	while (((parser.end - parser.pos) > 7) && !memcmp(parser.pos, "parent ", 7))
		rb_ary_push(parents, scan_line(&parser, "parent ", 7));
	rb_hash_aset(out, ID2SYM(rb_intern("parents")), parents);
	rb_hash_aset(out, ID2SYM(rb_intern("author")), scan_author(&parser, "author ", 7));
	rb_hash_aset(out, ID2SYM(rb_intern("committer")), scan_author(&parser, "committer ", 10));

	parser_advance(&parser, !((*parser.pos == '\n') && (*(parser.pos - 1) == '\n')));
	parser.pos++; /* skip '\n' */

	message.start = parser.pos;
	message.end = parser.end;
	rb_hash_aset(out, ID2SYM(rb_intern("message")), range_to_s(&message));

	return out;
}

static VALUE parse_tag(VALUE self, VALUE data) {
	struct parser parser;
	struct range message, signature, signed_text;
	VALUE out = rb_hash_new();

	parser_init(&parser, data);

	rb_hash_aset(out, ID2SYM(rb_intern("object")), scan_line(&parser, "object ", 7));
	rb_hash_aset(out, ID2SYM(rb_intern("type")), scan_line(&parser, "type ", 5));
	rb_hash_aset(out, ID2SYM(rb_intern("tag")), scan_line(&parser, "tag ", 4));
	rb_hash_aset(out, ID2SYM(rb_intern("tagger")), scan_author(&parser, "tagger ", 7));

	parser_advance(&parser, !((*parser.pos == '\n') && (*(parser.pos - 1) == '\n')));
	parser.pos++; /* skip '\n' */
	message.start = parser.pos;

	signature.start = 0;
	while ((parser.end - parser.pos) >= 11) {
		if (!memcmp(parser.pos, "\n-----BEGIN", 11)) {
			parser.pos++; /* skip '\n' */
			signature.start = parser.pos;
			break;
		}
		parser.pos++;
	}

	if (signature.start) {
		message.end = signature.start;
		rb_hash_aset(out, ID2SYM(rb_intern("message")), range_to_s(&message));
		signature.end = parser.end;
		rb_hash_aset(out, ID2SYM(rb_intern("signature")), range_to_s(&signature));
		signed_text.start = parser.start;
		signed_text.end = signature.start;
		rb_hash_aset(out, ID2SYM(rb_intern("signed_text")), range_to_s(&signed_text));
	} else {
		message.end = parser.end;
		rb_hash_aset(out, ID2SYM(rb_intern("message")), range_to_s(&message));
	}

	return out;
}

static VALUE parse_author(VALUE self, VALUE data) {
	struct parser parser;
	VALUE out;

	parser_init(&parser, data);

	out = scan_author(&parser, "", 0);
	if (parser.pos != parser.end)
		rb_raise(rb_eParseError, "trailing garbage after author line");
	return out;
}

void Init_cparser(void){
	VALUE rb_mGitParser = rb_define_module("GitParser");
	rb_eParseError = rb_define_class_under(rb_mGitParser, "ParseError", rb_eArgError);
	rb_define_module_function(rb_mGitParser, "parse_commit", parse_commit, 1);
	rb_define_module_function(rb_mGitParser, "parse_tag", parse_tag, 1);
	rb_define_module_function(rb_mGitParser, "parse_author", parse_author, 1);
}
